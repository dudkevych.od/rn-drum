import { NativeModules, Platform } from 'react-native';
import { DateDrum } from './DateDrum';

const LINKING_ERROR =
  `The package 'rn-drum' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo Go\n';

const RnDrum = NativeModules.RnDrum
  ? NativeModules.RnDrum
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

export { DateDrum };
