/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { Dimensions, View } from 'react-native';
import { DrumBlock } from '../components/DrumBlock';

const DateDrum = (props) => {
  const {
    height,
    width,
    format,
    startYear,
    endYear,
    initialDate,
    onChange,
    styles,
  } = props;

  const [days, setDays] = useState([]);
  const [months, setMonths] = useState([]);
  const [years, setYears] = useState([]);

  const pickerHeight = useRef(
    Math.round(height || Dimensions.get('window').height / 3.5)
  ).current;

  const pickerWidth = useRef(width || '100%').current;
  const dHeight = useRef(Math.round(pickerHeight / 7)).current;

  const style = useRef({
    picker: {
      flexDirection: 'row',
      width: '100%',
    },
    mark: {
      position: 'absolute',
      top: pickerHeight / 2 - dHeight / 2,
      left: 0,
      right: 0,
      height: dHeight,
      ...(styles?.mark ?? {}),
    },
  }).current;

  useEffect(() => {
    const end = endYear || new Date().getFullYear();
    const start = !startYear || startYear > end ? end - 100 : startYear;

    const _days = [...Array(31)].map((_, index) => index + 1);
    const _months = [...Array(12)].map((_, index) => index + 1);
    const _years = [...Array(end - start + 1)].map((_, index) => start + index);

    setDays(_days);
    setMonths(_months);
    setYears(_years);
  }, []);

  const unexpectedDate = new Date(years[0], 0, 1);
  const date = new Date(initialDate || unexpectedDate);

  const getOrder = useMemo(() => {
    if (!days.length || !months.length || !years.length) return [];

    const formated = format ?? 'dd-mm-yyyy'.split('-');

    return formated.map((type, index) => {
      switch (type) {
        case 'dd':
          return { name: 'day', digits: days, value: date.getDate() };
        case 'mm':
          return { name: 'month', digits: months, value: date.getMonth() + 1 };
        case 'yyyy':
          return { name: 'year', digits: years, value: date.getFullYear() };
        default:
          return {
            name: ['day', 'month', 'year'][index],
            digits: [days, months, years][index],
            value: [date.getDate(), date.getMonth() + 1, date.getFullYear()][
              index
            ],
          };
      }
    });
  }, [days, months, years]);

  const changeHandle = (type, digit) => {
    switch (type) {
      case 'day':
        date.setDate(digit);
        break;
      case 'month':
        date.setMonth(digit - 1);
        break;
      case 'year':
        date.setFullYear(digit);
        break;
    }

    onChange?.(date);
  };

  if (!getOrder.length) return;

  return (
    <View style={[style.picker, { height: pickerHeight, width: pickerWidth }]}>
      <View style={style.mark} />
      {getOrder.map((item) => {
        return (
          <DrumBlock
            digits={item.digits}
            value={item.value}
            type={item.name}
            height={pickerHeight}
            dHeight={dHeight}
            onChange={changeHandle}
            key={`${item.name}-${item.value}`}
            styles={style}
          />
        );
      })}
    </View>
  );
};

export { DateDrum };
