/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef, useState } from 'react';
import {
  Animated,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const DateDrum = (props) => {
  const {
    format,
    height = 300,
    width,
    value,
    endYear,
    startYear,
    onChange,
  } = props;

  const [days, setDays] = useState([]);
  const [months, setMonths] = useState([]);
  const [years, setYears] = useState([]);

  useEffect(() => {
    const end = endYear || new Date().getFullYear();
    const start = !startYear || startYear > end ? end - 100 : startYear;

    const _days = [...Array(31)].map((_, index) => index + 1);
    const _months = [...Array(12)].map((_, index) => index + 1);
    const _years = [...Array(end - start + 1)].map((_, index) => start + index);

    setDays(_days);
    setMonths(_months);
    setYears(_years);
  }, []);

  const pickerHeight = Math.round(
    height || Dimensions.get('window').height / 3.5
  );
  const pickerWidth = width || '100%';

  const unexpectedDate = new Date(years[0], 0, 1);
  const date = new Date(value || unexpectedDate);

  const changeHandle = (type, digit) => {
    switch (type) {
      case 'day':
        date.setDate(digit);
        break;
      case 'month':
        date.setMonth(digit - 1);
        break;
      case 'year':
        date.setFullYear(digit);
        break;
    }

    onChange?.(date);
  };

  const getOrder = () => {
    return (format || 'dd-mm-yyyy').split('-').map((type, index) => {
      switch (type) {
        case 'dd':
          return { name: 'day', digits: days, value: date.getDate() };
        case 'mm':
          return { name: 'month', digits: months, value: date.getMonth() + 1 };
        case 'yyyy':
          return { name: 'year', digits: years, value: date.getFullYear() };
        default:
          console.warn(
            `Invalid date picker format prop: found "${type}" in ${format}. Please read documentation!`
          );
          return {
            name: ['day', 'month', 'year'][index],
            digits: [days, months, years][index],
            value: [date.getDate(), date.getMonth() + 1, date.getFullYear()][
              index
            ],
          };
      }
    });
  };

  const DateBlock = ({
    value,
    digits,
    type,
    onChange,
    height,
    fontSize,
    textColor,
    markColor,
    markHeight,
    markWidth,
    // fadeColor,
  }) => {
    const dHeight = Math.round(height / 5);
    const scrollY = useRef(new Animated.Value(0)).current;

    const mHeight = markHeight || Math.min(dHeight, 65);
    const mWidth = markWidth || '70%';

    const offsets = digits.map((_, index) => index * dHeight);

    // const fadeFilled = hex2rgba(fadeColor || '#ffffff', 1);
    // const fadeTransparent = hex2rgba(fadeColor || '#ffffff', 0);

    const scrollRef = useRef(null);

    const snapScrollToIndex = (index) => {
      scrollRef?.current?.scrollToOffset({
        offset: dHeight * index,
        animated: true,
      });
    };

    useEffect(() => {
      snapScrollToIndex(value - digits[0]);
    }, [scrollRef.current]);

    const handleMomentumScrollEnd = ({ nativeEvent }) => {
      const digit = Math.round(
        nativeEvent.contentOffset.y / dHeight + digits[0]
      );
      onChange?.(type, digit);
    };

    return (
      <View style={styles.block}>
        {/* <View */}
        {/*   style={[ */}
        {/*     styles.mark, */}
        {/*     { */}
        {/*       top: (height - mHeight) / 2, */}
        {/*       backgroundColor: markColor || 'rgba(0, 0, 0, 0.5)', */}
        {/*       height: mHeight, */}
        {/*       width: mWidth, */}
        {/*     }, */}
        {/*   ]} */}
        {/* /> */}
        <Animated.FlatList
          ref={scrollRef}
          data={digits}
          keyExtractor={(item) => item.index}
          renderItem={({ item, index }) => {
            const inputRange = [
              (index - 2) * dHeight,
              (index - 1) * dHeight,
              index * dHeight,
              (index + 1) * dHeight,
              (index + 2) * dHeight,
            ];

            const opacity = scrollY.interpolate({
              inputRange,
              outputRange: [0.6, 0.8, 1, 0.8, 0.6],
            });
            const scale = scrollY.interpolate({
              inputRange,
              outputRange: [0.6, 0.85, 1, 0.85, 0.6],
            });
            return (
              <Animated.View
                style={{ opacity, transform: [{ scale }] }}
                key={`${type}-${item}`}
              >
                <TouchableOpacity
                  key={index}
                  onPress={() => {
                    onChange?.(type, item);
                    snapScrollToIndex(index);
                  }}
                >
                  <Text
                    style={[
                      styles.digit,
                      {
                        fontSize: fontSize || 22,
                        color: textColor || '#000000',
                        marginBottom:
                          index === digits.length - 1
                            ? height / 2 - dHeight / 2
                            : 0,
                        marginTop: index === 0 ? height / 2 - dHeight / 2 : 0,
                        lineHeight: dHeight,
                        height: dHeight,
                      },
                    ]}
                  >
                    {item}
                  </Text>
                </TouchableOpacity>
              </Animated.View>
            );
          }}
          style={styles.scroll}
          snapToOffsets={offsets}
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={16}
          onMomentumScrollEnd={handleMomentumScrollEnd}
          decelerationRate={0.98}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: scrollY } } }],
            { useNativeDriver: true }
          )}
        >
          {/* {digits.map((value, index) => { */}
          {/*   return ( */}
          {/*     <TouchableOpacity */}
          {/*       key={index} */}
          {/*       onPress={() => { */}
          {/*         onChange?.(type, digits[index]); */}
          {/*         snapScrollToIndex(index); */}
          {/*       }} */}
          {/*     > */}
          {/*       <Text */}
          {/*         style={[ */}
          {/*           styles.digit, */}
          {/*           { */}
          {/*             fontSize: fontSize || 22, */}
          {/*             color: textColor || '#000000', */}
          {/*             marginBottom: */}
          {/*               index === digits.length - 1 */}
          {/*                 ? height / 2 - dHeight / 2 */}
          {/*                 : 0, */}
          {/*             marginTop: index === 0 ? height / 2 - dHeight / 2 : 0, */}
          {/*             lineHeight: dHeight, */}
          {/*             height: dHeight, */}
          {/*           }, */}
          {/*         ]} */}
          {/*       > */}
          {/*         {value} */}
          {/*       </Text> */}
          {/*     </TouchableOpacity> */}
          {/*   ); */}
          {/* })} */}
        </Animated.FlatList>
        {/* <LinearGradient */}
        {/*   style={[styles.gradient, { bottom: 0, height: height / 4 }]} */}
        {/*   colors={[fadeTransparent, fadeFilled]} */}
        {/*   pointerEvents={'none'} */}
        {/* /> */}
        {/* <LinearGradient */}
        {/*   style={[styles.gradient, { top: 0, height: height / 4 }]} */}
        {/*   colors={[fadeFilled, fadeTransparent]} */}
        {/*   pointerEvents={'none'} */}
        {/* /> */}
      </View>
    );
  };

  return (
    <View style={[styles.picker, { height: pickerHeight, width: pickerWidth }]}>
      {getOrder().map((el, index) => {
        return (
          <DateBlock
            digits={el.digits}
            value={el.value}
            onChange={changeHandle}
            height={pickerHeight}
            fontSize={16}
            textColor={'#000'}
            markColor={'rgba(0,0,0, 0.5)'}
            markHeight={20}
            markWidth={100}
            // fadeColor={'rgba("#000", 0.1)'}
            type={el.name}
            key={index}
          />
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  picker: {
    flexDirection: 'row',
    width: '100%',
  },
  block: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  scroll: {
    width: '100%',
  },
  digit: {
    fontSize: 20,
    textAlign: 'center',
  },
  mark: {
    position: 'absolute',
    borderRadius: 10,
  },
  gradient: {
    position: 'absolute',
    width: '100%',
  },
});

export { DateDrum };
