/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { memo, useCallback, useEffect, useRef } from 'react';
import { Animated, Text, TouchableOpacity, View } from 'react-native';

const DrumBlock = memo(
  (props) => {
    const { height, digits, type, value, onChange, dHeight, styles } = props;

    const scrollY = useRef(new Animated.Value(0)).current;

    // const mHeight = markHeight || Math.min(dHeight, 65);
    // const mWidth = markWidth || '70%';

    const offsets = digits.map((_, index) => index * dHeight);

    // const fadeFilled = hex2rgba(fadeColor || '#ffffff', 1);
    // const fadeTransparent = hex2rgba(fadeColor || '#ffffff', 0);

    const scrollRef = useRef(null);

    const snapScrollToIndex = useCallback((index) => {
      scrollRef?.current?.scrollToOffset({
        offset: dHeight * index,
        animated: true,
      });
    }, []);

    const style = useRef({
      block: {
        flex: 1,
        ...(styles?.block ?? {}),
      },
      ...(styles[`block.${type}`] ?? {}),
      digit: {
        textAlign: 'center',
        fontSize: 22,
        color: '#000000',
        lineHeight: dHeight,
        height: dHeight,
        ...(styles?.digit ?? {}),
      },
    }).current;

    useEffect(() => {
      snapScrollToIndex(value - digits[0]);
    }, []);

    const handleMomentumScrollEnd = ({ nativeEvent }) => {
      const digit = Math.round(
        nativeEvent.contentOffset.y / dHeight + digits[0]
      );
      onChange?.(type, digit);
    };

    return (
      <View style={style.block}>
        <Animated.FlatList
          style={{ flex: 1 }}
          ref={scrollRef}
          data={digits}
          keyExtractor={(item, index) => `${type}-${item}-${index}`}
          renderItem={({ item, index }) => {
            const inputRange = [
              (index - 3) * dHeight,
              (index - 2) * dHeight,
              (index - 1) * dHeight,
              index * dHeight,
              (index + 1) * dHeight,
              (index + 2) * dHeight,
              (index + 3) * dHeight,
            ];

            const opacity = scrollY.interpolate({
              inputRange,
              outputRange: [0.75, 0.8, 0.9, 1, 0.9, 0.8, 0.75],
            });
            const scale = scrollY.interpolate({
              inputRange,
              outputRange: [0.6, 0.6, 0.85, 1, 0.85, 0.6, 0.6],
            });

            return (
              <Animated.View
                style={{
                  opacity,
                  transform: [{ scale }],
                  marginBottom:
                    index === digits.length - 1 ? height / 2 - dHeight / 2 : 0,
                  marginTop: index === 0 ? height / 2 - dHeight / 2 : 0,
                }}
                key={`${type}-${item}`}
              >
                <TouchableOpacity
                  key={`${type}-${item}`}
                  onPress={() => {
                    onChange?.(type, item);
                    snapScrollToIndex(index);
                  }}
                >
                  <Text style={style.digit}>{item}</Text>
                </TouchableOpacity>
              </Animated.View>
            );
          }}
          snapToOffsets={offsets}
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={16}
          onMomentumScrollEnd={handleMomentumScrollEnd}
          decelerationRate={0.98}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: scrollY } } }],
            { useNativeDriver: true }
          )}
        />
        {/* <LinearGradient */}
        {/*   style={[styles.gradient, { bottom: 0, height: height / 4 }]} */}
        {/*   colors={[fadeTransparent, fadeFilled]} */}
        {/*   pointerEvents={'none'} */}
        {/* /> */}
        {/* <LinearGradient */}
        {/*   style={[styles.gradient, { top: 0, height: height / 4 }]} */}
        {/*   colors={[fadeFilled, fadeTransparent]} */}
        {/*   pointerEvents={'none'} */}
        {/* /> */}
      </View>
    );
  },
  (prev, next) => {
    return prev.digits === next.digits;
  }
);

export { DrumBlock };
