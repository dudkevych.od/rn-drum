import * as React from 'react';

import { DateDrum } from 'rn-drum';

export default function App() {
  return <DateDrum />;
}
