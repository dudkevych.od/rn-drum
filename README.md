# rn-drum

drum

## Installation

```sh
npm install rn-drum
```

## Usage

```js
import { multiply } from 'rn-drum';

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT

---

Made with [create-react-native-library](https://github.com/callstack/react-native-builder-bob)
